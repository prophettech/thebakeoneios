//
//  Services.swift
//  Food Delivery
//
//  Created by Akram Hussain on 28/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation

class Services {
    
    static let consumerSecret = "cs_c792c8b83ad1b687302e16dab91aff16f159d0e3"
    static let consumerKey = "ck_b884f2df37e7dfe6bc2f358c60afcfe73bded2d5"
    
    enum Endpoints {
        static let base = "https://thebake1.co.uk/"
        
        case getAllProducts
        case getAllCategories
        case getProductsByCategory
        case getProductById
        case placeOrder
        case makePayment
        case addNote
        case orderStatus
        case insertToken
        
        var stringValue: String {
            switch self {
                
            case .getAllProducts: return Endpoints.base + "wp-json/wc/v3/products"
                + "?consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
                
            case .getAllCategories: return Endpoints.base + "wp-json/wc/v3/products/categories"
                + "?per_page=100&consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
                
            case .getProductsByCategory: return Endpoints.base + "wp-json/wc/v3/products?per_page=100&category="
                
            case .getProductById: return Endpoints.base + "wp-json/wc/v3/products/"
                
            case .placeOrder: return Endpoints.base + "wp-json/wc/v3/orders"
                + "?consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
                
            case .makePayment: return "https://api.worldpay.com/v1/orders"
                
            case .addNote: return Endpoints.base + "wp-json/wc/v3/orders/"
            
            case .orderStatus: return Endpoints.base + "fcm/orders.json"
            
            case .insertToken: return Endpoints.base + "fcm/inserttoken.php"
            }
        }
        
        var url: URL {
            return URL(string: stringValue)!
        }
    }
    
    
    class func getAllProducts(completion: @escaping (Product?) -> Void) {
        _ = taskForGETRequest(url: Endpoints.getAllProducts.url, responseType: Product.self) { response, error in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
    
    class func getAllCategories(completion: @escaping (Categories?) -> Void){
        _ = taskForGETRequest(url: Endpoints.getAllCategories.url, responseType: Categories.self) { response, error in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
    
    class func getProductsByCategory(id: String, completion: @escaping (Products?) -> Void){
        let urlValue = Endpoints.getProductsByCategory.stringValue + id + "&consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
        
        guard let url = URL(string: urlValue) else { return }
        
        _ = taskForGETRequest(url: url, responseType: Products.self) { (response, error) in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
    
    class func getProductById(id: String, completion: @escaping (ProductDetail?) -> Void){
        let urlValue = Endpoints.getProductById.stringValue + id + "?consumer_key=\(Services.consumerKey)" + "&consumer_secret=\(Services.consumerSecret)"
        
        guard let url = URL(string: urlValue) else { return }
        
        _ = taskForGETRequest(url: url, responseType: ProductDetail.self) { (response, error) in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
    
    class func addNotes(id: Int, body: NotesBody, completion: @escaping(NotesResponse?) -> Void) {
        let urlValue = Endpoints.addNote.stringValue + String(id) + "/notes" + "?consumer_key=\(Services.consumerKey)" +
        "&consumer_secret=\(Services.consumerSecret)"
        
        guard let url = URL(string: urlValue) else { return }
        
        taskForPOSTRequest(url: url, responseType: NotesResponse.self, body: body) { (response, error) in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
            
        }
    }
    
    class func placeOrder(body: PlaceOrder, completion: @escaping (OrderResponse?) -> Void){
        taskForPOSTRequest(url: Endpoints.placeOrder.url, responseType: OrderResponse.self, body: body) { (response, error) in
            if let response = response {
                completion(response)
            } else {
                completion(nil)
            }
        }
    }
        
    class func getOrderStatus(completion: @escaping(OrderStatusRespones) -> Void){
        _ = taskForGETRequest(url: Endpoints.orderStatus.url, responseType: OrderStatusRespones.self) { (response, error) in
            if let response = response {
                completion(response)
            }
        }
    }
    
    class func insertFirebaseToken(id: String,token: String, completion: @escaping(FirebaseTokenResponse) -> Void){
        let body = FirebaseToken(id: id, fcmtoken: token)
        taskForPOSTRequest(url: Endpoints.insertToken.url, responseType: FirebaseTokenResponse.self, body: body) { (response, error) in
            if let response = response{
                completion(response)
            }
        }
    }
    
    class func taskForGETRequest<ResponseType: Decodable>(url: URL, responseType: ResponseType.Type, completion: @escaping (ResponseType?, Error?) -> Void) -> URLSessionDataTask {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            let decoder = JSONDecoder()
            do {
                let responseObject = try decoder.decode(ResponseType.self, from: data)
                print("--------", responseObject)
                DispatchQueue.main.async {
                    completion(responseObject, nil)
                }
            } catch {
                do {
                    let errorResponse = try decoder.decode(Response.self, from: data) as Error
                    DispatchQueue.main.async {
                        completion(nil, errorResponse)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        }
        task.resume()
        
        return task
    }
    
    class func taskForPOSTRequest<RequestType: Encodable, ResponseType: Decodable>(url: URL, responseType: ResponseType.Type, body: RequestType, completion: @escaping (ResponseType?, Error?) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try! JSONEncoder().encode(body)
        print("--------", String(decoding: request.httpBody!, as: UTF8.self))
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            let decoder = JSONDecoder()
            do {
                let responseObject = try decoder.decode(ResponseType.self, from: data)
                DispatchQueue.main.async {
                    completion(responseObject, nil)
                }
            } catch {
                do {
                    let errorResponse = try decoder.decode(Response.self, from: data) as Error
                    DispatchQueue.main.async {
                        completion(nil, errorResponse)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        }
        task.resume()
    }
    
}
