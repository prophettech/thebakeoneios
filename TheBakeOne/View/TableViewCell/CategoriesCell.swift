//
//  CategoriesCell.swift
//  Food Delivery
//
//  Created by Akram Hussain on 28/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import UIKit
import Alamofire


class CategoriesCell : UITableViewCell
{
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var shortDescLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var categories: Category! {
        didSet{
            updateUI()
        }
    }
    
    var product: Product! {
        didSet {
            updateProducts()
        }
    }
    
    func updateUI()
    {
        self.nameLabel.text = categories.name
        self.descriptionLabel.text = categories.categoryDescription?.htmlToAttributedString?.string
        self.logoImageView.image = nil
        if let imageURL = URL(string: (categories.image?.src ?? "")) {
            (AF.request(imageURL)).responseData { (responseData) in
                DispatchQueue.main.async {
                    if let imageData = responseData.data {
                        self.logoImageView.image = UIImage(data: imageData)
                    }
                }
            }
        }
    }
    
    func updateProducts()
    {
        self.nameLabel.text = product.name
        self.descriptionLabel.text = product.productDescription?.htmlToAttributedString?.string
        
        if (product.images?.count ?? 0 > 0){
            self.logoImageView.image = nil
            if let imageURL = URL(string: (product?.images?[0].src ?? "")) {
                (AF.request(imageURL)).responseData { (responseData) in
                    DispatchQueue.main.async {
                        if let imageData = responseData.data {
                            self.logoImageView.image = UIImage(data: imageData)
                        }
                    }
                }
            }
        }
    }
}
