//
//  OptionsViewController.swift
//  Food Delivery
//
//  Created by Akram Hussain on 05/12/2019.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import UIKit
import OrderedDictionary

class OptionsViewController: UIViewController, UITableViewDataSource {
        
    @IBOutlet weak var tableView: UITableView!
   
    var optionsAndPrice = OrderedDictionary<String, String>()
    var selectedOptions = Dictionary<String, String>()
    var delegate: SelectedOptionDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionsAndPrice.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionsCell
        let item = Array(optionsAndPrice.orderedValues)[indexPath.row]
        cell.optionTitle.text = Array(optionsAndPrice.orderedKeys)[indexPath.row]
        if (item == ""){
            cell.optionPrice.text = " ￡" + item + "0"
        }else{
            cell.optionPrice.text = " ￡" + Util.formatNumber(Float(item)!)!
        }
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
}

extension OptionsViewController: OptionSelected {
    
    func optionSelected(title: String, price: String, selected: Bool) {
        if selected {
            selectedOptions[title] = price
        }
        if !selected {
            selectedOptions.removeValue(forKey: title)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.getSelectedOptions(options: selectedOptions)
    }
}

protocol SelectedOptionDictionary {
    func getSelectedOptions(options: Dictionary<String, String>)
}
