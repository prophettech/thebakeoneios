//
//  MyAPIClient.swift
//  Food Delivery
//
//  Created by Akram Hussain on 20/06/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import Foundation
import Alamofire
import Stripe

class MyAPIClient: NSObject {
        
    class func createPaymentIntent(amount:Double,currency:String, completion:@escaping (Result<String, AFError>)->Void){
        AF.request(URL(string: "https://www.thebake1.co.uk/stripebackend/createpaymentintent.php")!, method: .get, parameters: ["amount":amount,"currency":currency],encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .response { (response) in
                
            switch (response.result) {
            case .failure:
                print(response)
                completion(.failure(response.error!))
            case .success:
                print(response)
                let data = response.data
                guard let json = ((try? JSONSerialization.jsonObject(with: data!, options: []) as? [String : String]) as [String : String]??) else {
                    completion(.failure(response.error!))
                    return
                }
                completion(.success(json!["clientSecret"]!))
            }
        }
    }
}
