//
//  Response.swift
//  Food Delivery
//
//  Created by Akram Hussain on 30/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation

struct Response: Codable {
    let statusCode: String?
    let statusMessage: String?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "code"
        case statusMessage = "message"
    }
}

extension Response: LocalizedError {
    var errorDescription: String? {
        return statusMessage
    }
}
